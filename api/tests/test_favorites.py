from fastapi.testclient import TestClient
from main import app
from queries.favorites import FavoritesQueries
from models import FavoriteIn
from authenticator import authenticator

client = TestClient(app=app)


def fake_get_current_account_data():
    return {"id": "1337", "username": "fakeuser"}


class FakeFavoritesQueries:
    def get_all_for_pokemon_name(self, pokemon_name: str):
        return [
            {
                "pokemon_name": pokemon_name,
                "id": "64e4e87c3de6bf72206f09b4",
                "account_id": "64e4e7f13de6bf72206f09b3",
            },
        ]

    def get_all_for_account_id(self, account_id: str):
        return [
            {
                "pokemon_name": "pikachu",
                "id": "64e4e87c3de6bf72206f09b4",
                "account_id": account_id,
            },
        ]

    def create(self, favorite_in: FavoriteIn, account_id: str):
        favorite = favorite_in.dict()
        favorite["account_id"] = account_id
        favorite["id"] = "idfrommongowhocares"
        return favorite

    def delete(self, id: str, account_id: str):
        return True


def test_list_favorites_for_pokemon():
    app.dependency_overrides[FavoritesQueries] = FakeFavoritesQueries
    res = client.get("/api/pokemon/pikachu/favorites")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "favorites": [
            {
                "pokemon_name": "pikachu",
                "id": "64e4e87c3de6bf72206f09b4",
                "account_id": "64e4e7f13de6bf72206f09b3",
            },
        ]
    }


def test_list_favorites_for_current_account():
    app.dependency_overrides[FavoritesQueries] = FakeFavoritesQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    res = client.get("/api/favorites/mine")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "favorites": [
            {
                "pokemon_name": "pikachu",
                "id": "64e4e87c3de6bf72206f09b4",
                "account_id": "1337",
            },
        ]
    }


def test_create_favorite():
    app.dependency_overrides[FavoritesQueries] = FakeFavoritesQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    body = {"pokemon_name": "ditto"}
    res = client.post("/api/favorites", json=body)
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "pokemon_name": "ditto",
        "id": "idfrommongowhocares",
        "account_id": "1337",
    }


def test_delete_favorite():
    app.dependency_overrides[FavoritesQueries] = FakeFavoritesQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.delete("/api/favorites/idfrommongowhocares")
    data = res.json()

    assert res.status_code == 200
    assert data == {"success": True}
