from fastapi.testclient import TestClient
from main import app
from queries.pokemon import PokemonQueries

client = TestClient(app)


class FakePokemonQueries:
    def get_all(self, limit: int, offset: 0):
        return [
            {
                "name": "bulbasaur",
                "url": "https://pokeapi.co/api/v2/pokemon/1/",
            },
        ]

    def get_one_by_name(self, name: str):
        return {
            "abilities": [
                {
                    "ability": {
                        "name": "static",
                        "url": "https://pokeapi.co/api/v2/ability/9/",
                    },
                    "is_hidden": False,
                    "slot": 1,
                },
                {
                    "ability": {
                        "name": "lightning-rod",
                        "url": "https://pokeapi.co/api/v2/ability/31/",
                    },
                    "is_hidden": True,
                    "slot": 3,
                },
            ],
            "base_experience": 112,
            "height": 4,
            "id": 25,
            "is_default": True,
            "location_area_encounters": "location_area_encounters_url",
            "name": name,
            "order": 35,
            "weight": 60,
        }


def test_get_all_pokemon():
    app.dependency_overrides[PokemonQueries] = FakePokemonQueries
    res = client.get("/api/pokemon")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "pokemon": [
            {
                "name": "bulbasaur",
                "url": "https://pokeapi.co/api/v2/pokemon/1/",
            },
        ]
    }


def test_get_pokemon_by_name():
    app.dependency_overrides[PokemonQueries] = FakePokemonQueries
    res = client.get("/api/pokemon/pikachu")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "abilities": [
            {
                "ability": {
                    "name": "static",
                    "url": "https://pokeapi.co/api/v2/ability/9/",
                },
                "is_hidden": False,
                "slot": 1,
            },
            {
                "ability": {
                    "name": "lightning-rod",
                    "url": "https://pokeapi.co/api/v2/ability/31/",
                },
                "is_hidden": True,
                "slot": 3,
            },
        ],
        "base_experience": 112,
        "height": 4,
        "id": 25,
        "is_default": True,
        "location_area_encounters": "location_area_encounters_url",
        "name": "pikachu",
        "order": 35,
        "weight": 60,
    }
