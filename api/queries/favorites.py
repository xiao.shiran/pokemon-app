from bson.objectid import ObjectId
from queries.client import MongoQueries
from models import FavoriteIn


class FavoritesQueries(MongoQueries):
    collection_name = "favorites"

    def create(self, favorite_in: FavoriteIn, account_id: str):
        favorite = favorite_in.dict()
        favorite["account_id"] = account_id
        search = self.find_one_by_account_id_and_pokemon_name(
            account_id, favorite_in.pokemon_name
        )
        if search:
            return search
        result = self.collection.insert_one(favorite)
        if result.inserted_id:
            favorite["id"] = str(result.inserted_id)
            return favorite

    def find_one_by_account_id_and_pokemon_name(
        self, account_id: str, pokemon_name: str
    ):
        result = self.collection.find_one(
            {"account_id": account_id, "pokemon_name": pokemon_name}
        )
        if result is not None:
            result["id"] = str(result["_id"])
        return result

    def get_all_for_account_id(self, account_id: str):
        results = []
        for doc in self.collection.find({"account_id": account_id}):
            doc["id"] = str(doc["_id"])
            results.append(doc)
        return results

    def get_all_for_pokemon_name(self, pokemon_name: str):
        results = []
        for doc in self.collection.find({"pokemon_name": pokemon_name}):
            doc["id"] = str(doc["_id"])
            results.append(doc)
        return results

    def delete(self, id: str, account_id: str):
        result = self.collection.delete_one(
            {"_id": ObjectId(id), "account_id": account_id}
        )
        return result.deleted_count > 0
