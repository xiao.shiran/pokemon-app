import { useState } from "react";
import { useParams } from "react-router-dom";
import { useGetPokemonByNameQuery, useGetFavoritesForPokemonQuery, useGetTokenQuery } from './app/apiSlice';
import { Link } from "react-router-dom";

import FavoriteButtons from "./FavoriteButtons";

const PokemonDetails = () => {
    const { data: account } = useGetTokenQuery();
    const { name } = useParams();
    const { data: pokemon, isLoading } = useGetPokemonByNameQuery(name);
    const { data: favorites, isLoading: isLoadingFavorites } = useGetFavoritesForPokemonQuery(name);

    if (isLoading || isLoadingFavorites) return <div>Loading...</div>

    return (
        <div>
            <div className="row">
                <div className="col-8">
                    <h1>{pokemon.name.toUpperCase()}</h1>
                </div>
                <div className="col-4 text-end">
                    {account ? <FavoriteButtons name={name} /> : <Link to={'/login'} className="btn btn-outline-primary">Login</Link>}
                </div>
            </div>
            <ul className="list-group">
                <li className="list-group-item">
                    Favorites: {favorites.length}
                </li>
                <li className="list-group-item">
                    Height: {pokemon.height}
                </li>
                <li className="list-group-item">
                    Weight: {pokemon.weight}
                </li>
                <li className="list-group-item">
                    Order: {pokemon.order}
                </li>
            </ul>
        </div>
    )
}

export default PokemonDetails;
