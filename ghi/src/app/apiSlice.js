import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const pokemonApi = createApi({
    reducerPath: 'pokemonApi',
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_API_HOST
    }),
    endpoints: (builder) => ({
        getAllPokemon: builder.query({
            query: () => '/api/pokemon',
            transformResponse: (response) => response.pokemon
        }),
        getPokemonByName: builder.query({
            query: (name) => `/api/pokemon/${name}`
        }),
        getFavoritesForPokemon: builder.query({
            query: (name) => `/api/pokemon/${name}/favorites`,
            transformResponse: (response) => response.favorites,
            providesTags: (response, error, arg) => {
                return [{type: 'Favorites', id: arg}]
            }
        }),
        getFavoritesForAccount: builder.query({
            query: () => ({
                url: '/api/favorites/mine',
                credentials: 'include'
            }),
            transformResponse: (response) => response.favorites,
            providesTags: [{type: 'Favorites', id: 'MINE'}]
        }),
        deleteFavorite: builder.mutation({
            query: (favorite) => ({
                url: `/api/favorites/${favorite.id}`,
                method: 'DELETE',
                credentials: 'include'
            }),
            invalidatesTags: (response, error, arg) => {
                console.log({response, error, arg})
                return [
                    {type: 'Favorites', id: 'MINE'},
                    {type: 'Favorites', id: arg.pokemon_name}
                ]
            }
        }),
        createFavorite: builder.mutation({
            query: (body) => ({
                url: '/api/favorites',
                body,
                method: 'POST',
                credentials: 'include'
            }),
            invalidatesTags: (response, error, arg) => {
                return [
                    {type: 'Favorites', id: 'MINE'},
                    {type: 'Favorites', id: arg.pokemon_name}
                ]
            }
        }),
        getToken: builder.query({
            query: () => ({
                url: `/token`,
                credentials: 'include'
            }),
            transformResponse: (response) => response?.account || null,
            providesTags: ['Account']
        }),
        logout: builder.mutation({
            query: () => ({
                url: `/token`,
                method: 'DELETE',
                credentials: 'include'
            }),
            invalidatesTags: ['Account', 'Favorites']
        }),
        login: builder.mutation({
            query: (info) => {
                const formData = new FormData();
                formData.append('username', info.username)
                formData.append('password', info.password);
                return {
                    url: '/token',
                    method: 'POST',
                    body: formData,
                    credentials: 'include'
                }
            },
            invalidatesTags: ['Account', {type: 'Favorites', id: 'MINE'}]
        }),
        signup: builder.mutation({
            query: (body) => ({
                url: `/api/accounts`,
                body,
                method: 'POST',
                credentials: 'include'
            }),
            invalidatesTags: ['Account', {type: 'Favorites', id: 'MINE'}]
        })
    })
});


export const {
    useGetAllPokemonQuery,
    useGetPokemonByNameQuery,
    useGetFavoritesForPokemonQuery,
    useGetFavoritesForAccountQuery,
    useDeleteFavoriteMutation,
    useCreateFavoriteMutation,
    useGetTokenQuery,
    useLogoutMutation,
    useLoginMutation,
    useSignupMutation,

} = pokemonApi;
