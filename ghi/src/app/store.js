import { configureStore } from '@reduxjs/toolkit'

import { pokemonApi } from './apiSlice';
import searchReducer from './searchSlice';


export const store = configureStore({
  reducer: {
    search: searchReducer, // front end state
    [pokemonApi.reducerPath]: pokemonApi.reducer // api state
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(pokemonApi.middleware)
})
